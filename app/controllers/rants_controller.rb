class RantsController < ApplicationController
    before_filter :categories
	def index
		@rants = Rant.includes(:comments).order("created_at desc").limit(4)
        @category = Category.all
        @rant = Rant.find_by(verbum: true)
        @comments = Comment.all
        @rantp = Rant.order("comments_count DESC").limit(25)
	end
	def show 
    	@rants = Rant.find(params[:id])
    	@category = @rants.comments
        @user = @rants.user
        @comments = @rants.comments
        @comment = Comment.new
    end

    def new
    if user_signed_in?
        @category_options = Category.all.map{|c| [ c.title, c.id] }
    	@rant = Rant.new
    else
        redirect_to new_user_registration_path
    end
    end
    def upvote
        @rant = Rant.find(params[:id])
        @rant.upvote_by current_user
        redirect_to rant_path(@rant)
    end
    def downvote
        @rant = Rant.find(params[:id])
        @rant.downvote_by current_user
        redirect_to rant_path(@rant)
    end
    def create 
        @category = Category.all
        @rant = Rant.new(rant_params)
        @rant.user = current_user
        if @rant.save
            flash[:notice] = "Rant Successfully Heard"
            redirect_to @rant
        else
            flash[:error] = ":( not saved"
                render :new
        end
    end
    private
    def rant_params
        params.require(:rant).permit(:title, :body, :category_id, :rantpic)
    end
end
