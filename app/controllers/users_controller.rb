class UsersController < ApplicationController
	before_filter :categories

	def show 
		@user = User.find(params[:id])

	end
	def edit 
		if current_user
		@user = User.find(params[:id])
		else 
		redirect_to root_path
		end
	end

	def update
		@user = User.find(params[:id])
		@user.update_attributes(user_params)
		redirect_to user_path

	end
	private

	def user_params
		params.require(:user).permit(:firstname, :lastname, :username, :userbio, :userwebsite, :userfacebook, :usertwitter, :company_name, :status, :userlinkedin, :avatar)
	end
end
