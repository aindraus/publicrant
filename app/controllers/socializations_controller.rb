class SocializationsController < ApplicationController
	def like
		@rant = Rant.find(params[:id])
		current_user.like!(@rant)
		redirect_to rant_path(@rant)
	end

	def unlike
		@rant = Rant.find(params[:id])
		current_user.unlike!(@rant)
		redirect_to rant_path(@rant)
	end
end
