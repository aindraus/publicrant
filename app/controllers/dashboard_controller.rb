class DashboardController < ApplicationController
	before_filter :categories
	def index
		if current_user.admin == true 
			@users = User.all
		else
			redirect_to root_path
		end
	end
end
