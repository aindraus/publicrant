class CommentsController < ApplicationController


	def create
		@comment = Comment.new(comment_params)
		@comment.user_id = current_user.id
		@comment.save

		redirect_to rant_path(@comment.rant)
	end
	def index
		@comment = Comment.all
	end


	def show 
		@comment = Comment.find(params[:id])
	end

	private

	def comment_params
		params.require(:comment).permit(:body, :user_id, :rant_id)
	end
end
