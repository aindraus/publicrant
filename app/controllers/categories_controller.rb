class CategoriesController < ApplicationController
  before_filter :categories

    def index 
    	@category = Category.all
    	@rants = Rant.all.order("created_at DESC").limit(50)
    end 

    def new
      if current_user.admin == true 
        @category = Category.new
      else
        redirect_to root_path
      end
    end

    
    def edit
        @category = Category.find(params[:id])
    end

    def show 
    	@category = Category.find(params[:id])

    end

    def create
      @category = Category.new(category_params)

      if @category.save
        flash[:notice] = "Saved successfully"
        redirect_to new_category_path
      else
        flash[:error] = "There were errors"
        render :action => :new
      end
      #redirect_to @category
    end

    def update
      @category = Category.find(params[:id])

      #if @category.update(params[:category].permit(:category_params))
      #  redirect_to @category
      #else
      #  render 'edit'
      #end
    end

    def destroy
      @category = Category.find(params[:id])
      @category.destroy

      #redirect_to categorys_path
    end

    private
      def category_params
        params.require(:category).permit(:title, :description)
      end 
end