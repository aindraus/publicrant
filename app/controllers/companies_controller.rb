class CompaniesController < ApplicationController
	before_filter :categories
	def index
		@user = current_user
		if @user.company_id != nil 
		@companies = Company.find(params[:id])
		@company = @companies.user_id == current_user.id
		else
			redirect_to new_company_path
		end
	end

	def show
		@company = Company.find(params[:id])
	end

	def new
		if user_signed_in?
	    	@company = Company.new
	    else
	        redirect_to new_user_session_path
	    end
	end

	def create 
		@company = Company.new(company_params)
		@comapny.user_id = current_user.id
		if @company.save
			redirect_to company_path(@company)
		end
	end

	def edit
		if user_signed_in?
			@company = Company.find(params[:id])
				if @company.user_id == current_user.id
					@company = Company.find(params[:id])
				else
					redirect_to root_path
				end
		else
			redirect_to new_user_session_path
		end

	end

	private

	def company_params
		params.require(:company).permit(:name, :logo, :description, :website, :facebook, :twitter, :linkedin, :email, :user_id)
	end
end
