$(function(){
	var menu 	=$('#menu');
		bar		=$('.categort-bar');
		barHeight = bar.height();

		$(menu).on('click', function(e) {
			e.preventDefault();
			bar.slideToggle();
		});

		$(window).resize(function() {
			var w = $(window).width();
			if (w > 600 && bar.is(':hidden')) {
				bar.removeAttr('style');
			}
		});
});