# encoding: utf-8

class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::Compatibility::Paperclip

  storage :aws

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end


end
