class Company < ActiveRecord::Base
	has_many :rants
	has_many :comments
	has_many :users
	has_many :likes
	has_many :liked_things
end
