class Rant < ActiveRecord::Base
	belongs_to :category
	validates :category_id, presence: true
	has_many :comments
	belongs_to :user
	acts_as_votable
	mount_uploader :rantpic, RantpicUploader
end
