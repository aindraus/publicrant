class AddCommentCounterToRants < ActiveRecord::Migration
  def self.up
  	add_column :rants, :comments_count, :integer, :null => false, :default => 0

  	Rant.reset_column_information
  	Rant.all.each do |p|
  		p.update_attribute :comments_count, p.comments.length
  	end
  end

  def self.down
  	remove_column :rants, :comments_count
  end
end
