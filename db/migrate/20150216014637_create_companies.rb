class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
    	t.string :name
    	t.text :description
    	t.string :website
    	t.string :facebook
    	t.string :twitter
    	t.string :linkedin
    	t.string :email
    	t.integer :user_id
      t.timestamps null: false
    end
  end
end
