class RemoveSocialize < ActiveRecord::Migration
  def change
  	drop_table :likes
  	drop_table :mentions
  	drop_table :follows
  end
end
