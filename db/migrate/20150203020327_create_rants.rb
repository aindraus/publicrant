class CreateRants < ActiveRecord::Migration
  def change
    create_table :rants do |t|
    	t.string :title
    	t.string :media_url
    	t.text :body
    	t.integer :category_id
    	t.boolean :verbum

      t.timestamps null: false
    end
  end
end
